
from random import randint
from random import choice
import numpy as np
import random
import pandas as pd
import os
from datetime import datetime

os.chdir("/Users/elisemollet/Downloads")
cb = pd.read_csv("Coffeebar_2013-2017.csv", ";")

"""CLASS SHOP"""
class Shop (object): # acces to the foods and drinks list sold in the coffee bar, their prices and their probabilities
    def __init__(self, foodPrice, drinksPrice, probaFood, probaDrinks):
        self.foodPrice = foodPrice
        self.drinksPrice = drinksPrice
        self.historique = [] # sales history
        self.money = 0
        self.probaFood = probaFood # probabilities from our previous dataframe for each hour
        self.probaDrinks = probaDrinks

    foodPrice = {"sandwich": 5, "cookie": 2} #Price of one sandwich is 5 euros and one cookie costs 2 euros
    def getFoodPrice(self, foodName):
        if (foodName == "nothing"): #If customers buy nothing, cost is zero
            return 0
        if (foodName) in self.foodPrice:
            return self.foodPrice[foodName]
        else:
            return 3 # If customer buy another food than sandwich or cookie, the cost is 3 euros

    drinksPrice = {"milkshake": 5, "frappucino": 4, "water": 2} # One milkshake costs 5 euros, a frappucino costs 4 euros and water costs 2 euros
    def getdrinksPrice(self, drinksName):
        if drinksName in self.drinksPrice:
            return self.drinksPrice[drinksName]
        else:
            return 3 # If customer buy another drink than milkshake, frappucino and water, the cost is 3 euros

    def buyDrinks(self, hour): #Customers buy a first a drink, following the probabilities of class Shop
        currentProba = self.probaDrinks[self.probaDrinks['HOUR']==hour] # We calculate our cumulated probabilities with a loop
        choice = randint (0,100)# We choose an a random number

        allDrinks = list(currentProba.colums) # columns names are names of each probabilities
        allDrinks.pop (0)

        proba = np.delete (currentProba.values[0], 0)
        listProba = []
        for p in proba:
            listProba.append(p)
        index = np.random.choice (len(allDrinks),1, p=listProba)
        return allDrinks[index[0]]

    def buyFood(self, hour):
        currentProba = self.probaFood[self.probaFood['HOUR'] == hour] # We calculate our cumulated probabilities with a loop
        choice = randint(0, 100) # We choose an a random number

        allFood = list(currentProba.columns)
        allFood.pop(0)

        proba = np.delete(currentProba.values[0], 0)
        listProba = []
        for p in proba:
            listProba.append(p)

        index = np.random.choice(len(allFood), 1, p=listProba)

        print(index)
        print(allFood[index[0]])
        return allFood[index[0]]

    def receiveCustomer(self, hour, customer):
        food = self.buyFood(hour) #which food the customers buy
        drink = self.buyDrinks(hour) #which drink the customers buy
        priceFood = self.getFoodPrice(food) #the price of the food the customers buy
        priceDrink = self.getDrinksPrice(drink) #the price of the drinks the customers buy
        customer.buy(priceFood + priceDrink, food, drink) # we describe the command
        self.money += (priceFood + priceDrink) #amount of money spend by the customers for food and drinks
        if (isinstance(customer, TipCustomer)): # if it's a tipcustomer
            self.money += customer.tip() # we add the tip to the amount of money spend
        self.historique.append([customer.id, hour, food, drink]) #track of the history of purchases (food, drinks and hour).

    def mostExpensiveOrder(self):
            return 10 #because the most expensive order possible is 5 for a sandwich plus 5 for a milkshake


"""CLASS CLIENT"""

class Customer(object): # acces to the coffee bar customers (id), their budget, what they buy and if they are returning customers or not
    def init (self, money, idCli, returning):
        self.money = money # we calculate budget only if the customer is a returning customer, because their budget will decrease purchase after purchase. When they have not enough money to buy the more expensive product, we remove it from our base of 1000 customers
        self.foodBuy = []
        self.drinksBuy = []
        self.id = idCli
        self.returning = returning # True if they come back, false if not

    def buy(self,priceTot,nameFood,nameDrink):

        self.money = self.money - priceTot
        self.drinksBuy.append(nameDrink)

        if (nameFood != "Nothing"):
            self.foodBuy.append(nameFood)

class TipCustomer(Customer): # we analyse if customer give a random tip between 1 to 10 euro’s.
    def __init__(self, money, idCli): # here there are only two arguments beacause we know that the customer will not come back, so returning = false
        Customer.__init__(self, money, idCli, False)

    def tip(self):
        a=randint(1,10) # random tip between 1 to 10 euro’s.
        self.money -= a
        return a

class Simulation:
    def __init__(self):
        self.customersReturning = []
        self.allCustomers = []
        self.idDepart = 5000

allCustomers = []
customersReturning = []
idDepart = 5000
idCli = str("CID"+str(idDepart))

for i in range (0,1000):
    choice = randint (1,3)
    idDepart += 1

    if choice == 1:
        c = Customer.__init__ (500, idCli, True)
        customersReturning.append(c)
    else:
        c = Customer.__init__(250, idCli, True)
        customersReturning.append(c)
print (len(customersReturning))


def getCustomer(listReturning,idDepart):
    proba = randint(1,10)

    if (proba<=2):
        return choice(listReturning)
    else:
        proba = randint(1,10)
        idDepart +=1
        if (proba ==1):
            return TipCustomer.__init__(100, idCli)
        else:
            return Customer.__init__(100, idCli, False)


#Open our probability files

os.chdir("/Users/elisemollet/Downloads")
probaFood = pd.read_csv("proba_food.csv", sep=",")
print(probaFood)
probaDrinks = pd.read_csv("proba_drink.csv", sep=",")
print(probaDrinks)


shop = Shop(foodPrice, drinksPrice, probaFood, probaDrinks)

# Simulation
for row in probaFood.iterrows():
    hour = row[1]
    cus = getCustomer(customersReturning, idDepart)
    idDepart += 1
    shop.receiveCustomer(hour, cus)
#We test if a customer could be reuse
    if(cus.returning):
        if(cus.money < shop.mostExpensiveOrder()):
            print("todo")
#remove client from returningCustomers
    else:
        allCustomers.append(cus)

print(shop.historique)

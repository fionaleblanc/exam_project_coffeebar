#import different modules we need
import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt

# open the csv file
os.chdir("C:\Users\maya1\Documents")
cb = pd.read_csv("Coffeebar_2013-2017.csv", ";")

#ordering the file
cb['HOUR'] = cb['TIME'].str[-5:]
cb['DATE'] = pd.to_datetime(cb['TIME'].str[0:8])
cb.index = cb['DATE']
cb.pop('TIME')
cb.pop ('DATE')
cb = cb[['HOUR', 'CUSTOMER', 'DRINKS', 'FOOD']]
print (cb)

#PARTIE 1
#Question 1

print(cb.FOOD.unique()) #different kind of food sold by the coffee bar
print(cb.DRINKS.unique()) #different kind of drinks sold by the coffee bar
print(cb.CUSTOMER.nunique()) #number of unique custommer/id

#Question2
#Graph of sold food per year
y = [cb['2013']['FOOD'].count(),cb['2014']['FOOD'].count(),cb['2015']['FOOD'].count(),cb['2016']['FOOD'].count(),cb['2017']['FOOD'].count()]
x = ['2013','2014','2015','2016','2017']

width = 1/2
plt.bar(x,y,width,color="blue")
plt.xlabel('YEAR')
plt.ylabel('AMOUNT OF FOOD')
axes = plt.gca()
axes.set_ylim(32500, 33100)
plt.title("Sold food between 2013 and 2017")
plt.show()

#Graph of sold drinks per year
y = [cb['2013']['DRINKS'].count(),cb['2014']['DRINKS'].count(),cb['2015']['DRINKS'].count(),cb['2016']['DRINKS'].count(),cb['2017']['DRINKS'].count()]
x = ['2013','2014','2015','2016','2017']
width = 1/2
plt.xlabel('YEAR')
plt.ylabel('AMOUNT OF DRINKS')
axes = plt.gca()
axes.set_ylim(62400, 62420)
plt.bar(x,y,width,color="pink")

#Replacing of value "NaN" by "nothing"
cb['FOOD'].fillna("nothing", inplace=True)

#Question 3: Probabilities
proba_food = (cb.groupby(['HOUR', 'FOOD']).count()/ cb.groupby('HOUR').count())['CUSTOMER'] #probabilities group by hour and food
proba_food = proba_food.unstack() #Transform our serie in a dataframe
df1= pd.DataFrame(proba_food)
print(df1)
df1.to_csv('proba_food.csv',encoding='utf-8') #Saving the dataframe as a csv file

proba_drink = (cb.groupby(['HOUR', 'DRINKS']).count()/ cb.groupby('HOUR').count())['CUSTOMER'] #probabilities group by hour and drink
proba_drink = proba_drink.unstack() #Transform our serie in a dataframe
df2= pd.DataFrame(proba_drink)
print(df2)
df2.to_csv('proba_drink.csv',encoding='utf-8') #Saving the dataframe as a csv file